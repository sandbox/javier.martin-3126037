<?php

use Drupal\Component\Utility\SafeMarkup;

/**
 * Implements hook_install().
 *
 * Add roles.
 */
function efichajes_install() {
  $messenger = \Drupal::service('messenger');
  
  // Create efichajes roles and assign his own permission.
  $role = \Drupal\user\Entity\Role::create([
    'id' => 'efichajes_admin', 
    'label' => 'efichajes - Admin']);
  $role->grantPermission('access efichajes config');
  $role->grantPermission('access signing types');
  $role->grantPermission('add signing type');
  $role->grantPermission('alter signing type');
  $role->grantPermission('delete signing type');
  $role->grantPermission('access worker');
  $role->grantPermission('add worker');
  $role->grantPermission('alter worker');
  $role->grantPermission('delete worker');
  $role->grantPermission('access calendar');
  $role->grantPermission('add calendar');
  $role->grantPermission('alter calendar');
  $role->grantPermission('delete calendar');
  $role->grantPermission('access date range');
  $role->grantPermission('add date range');
  $role->grantPermission('alter date range');
  $role->grantPermission('delete date range');
  $role->save();
  
  $messenger->addStatus(t('@role rol created.', [
    '@role' => $role->get('label')]));
  
  $role = \Drupal\user\Entity\Role::create(array('id' => 'efichajes_worker', 'label' => 'efichajes - Worker'));
  $role->grantPermission('access my dashboard');
  $role->grantPermission('access my signings');
  $role->grantPermission('add signing');
  
  $role->save();
  $messenger->addStatus(t('@role rol created.', [
    '@role' => $role->get('label')]));
  
  // Add worker menu
  $menu_storage = Drupal::entityTypeManager()->getStorage('menu');
  $menu = $menu_storage->create([
    'id' => 'efichajes_worker',
    'label' => 'Worker menu',
    'description' => 'eFichajes - Worker menu',
  ]);
  $menu->save();
  $messenger->addMessage(t('Added menu "Worker menu"'));
}

/**
 * Implements hook_uninstall().
 *
 * Remove roles.
 */
function efichajes_uninstall(){
  $messenger = \Drupal::service('messenger');
  
  // Remove efichajes roles
  $roles = \Drupal\user\Entity\Role::loadMultiple(array('efichajes_admin', 'efichajes_worker'));
  foreach ($roles as $role) {
    $rol_label = $role->get('label');
    $role->delete();
    $messenger->addStatus(t('@role rol removed.', [
      '@role' => $rol_label ]));
  }
  
  // Remove worker menu
  $menu_storage = Drupal::entityTypeManager()->getStorage('menu');
  $menu = $menu_storage->load('efichajes_worker');
  $menu->delete();
  $messenger->addMessage(t('Menu "Worker menu" deleted.'));
}

/**
 * Implements hook_cron().
 */
function efichajes_cron() {
  $datetime = new DateTime();
  $datetime->sub(new DateInterval('P1M'));
  
  $first_day = new DateTime(); $first_day->setTimestamp($datetime->getTimestamp()); 
  $first_day->modify('first day of this month');
  $first_day->setTime(0,0,0);
  
  $last_day = new DateTime(); $last_day->setTimestamp($datetime->getTimestamp());
  $last_day->modify('last day of this month');
  $last_day->setTime(23,59,59);
  
  $efichajesManager = \Drupal::service('efichajes.manager');
  
  $workers = $efichajesManager->getWorkers();
  foreach ($workers as $key => $value) {
    $total_theoreticaltime = 
      $efichajesManager->getTheoreticalTimePerPeriod($value, $first_day->getTimestamp(), $last_day->getTimestamp());
    $total_worktime = 
      $efichajesManager->getWorkTimePerPeriod($value, $first_day->getTimestamp(), $last_day->getTimestamp());
    
      if ($total_worktime < $total_theoreticaltime) {
        \Drupal::logger('efichajes')->info('Enviar correo ' . $value->getUsername());
        $mailManager = \Drupal::service('plugin.manager.mail');
        $params = [];
        $params['worker'] = $value;
        $languaje_code = \Drupal::languageManager()->getDefaultLanguage();
        $send_now = TRUE;
        
        $result = $mailManager->mail('efichajes', 'time_deficit', 
            $value->getEmail(), $languaje_code, $params, NULL, $send_now);
        
        \Drupal::logger('efichajes')->info('Send deficit worktime mail to ' . 
            $value->getUsername());
      }
  }  
}

/**
 * Implement hook_mail().
 */
function efichajes_mail($key, &$message, $params) {
  $options = array(
    'langcode' => $message['langcode'],
  );
  
  switch ($key) {
    case 'time_deficit':
      $worker = $params['worker'];
      $message['from'] = \Drupal::config('system.site')->get('mail');
      $message['subject'] = t('Efichajes - Deficit work time');
      $message['body'][] = t('Dear @surname, @name:', [
        '@surname' => $worker->get('field_ef_surname')->value,
        '@name' => $worker->get('field_ef_name')->value,
      ]);
      $message['body'][] = t('Last month you have worktime deficit. Please, review your signings.');
      $message['body'][] = t('Greetings.');
      break;
  }
}

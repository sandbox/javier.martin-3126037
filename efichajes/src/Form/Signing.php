<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\efichajes\EfichajesFormatterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Signing extends FormBase {
  protected $efichajesManager;
  protected $efichajesFormatter;
  protected $messenger;
  protected $current_user;
  protected $requestStack;
  
  /**
   * Construct implementation.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      EfichajesFormatterInterface $efichajesFormatter, 
      MessengerInterface $messenger, AccountProxyInterface $current_user,
      RequestStack $requestStack) {
    $this->efichajesManager = $efichajesManager;
    $this->efichajesFormatter = $efichajesFormatter;
    $this->messenger = $messenger;
    $this->current_user = $current_user;
    $this->requestStack = $requestStack;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\Signing
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter'),
        $container->get('messenger'),
        $container->get('current_user'),
        $container->get('request_stack'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'SigningForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user = $this->efichajesManager->getUserById($this->current_user->id());
    
    if (empty($user->get('field_ef_calendar_id')->getValue())) {
      $form_state->setErrorByName('calendar', $this->t('Sorry, your user has no calendar set.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $date = new \DateTime();
    $date_start = new \DateTime(); $date_start->setTime(0, 0, 0);
    $date_end = new \DateTime(); $date_end->setTime(23, 59, 0);
    $user = $this->efichajesManager->getUserById($this->current_user->id());
    
    $form['time'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => $date->format('d/m/Y H:i:s'),
      '#weight' => 1,
    ];
    
    $form['field_ef_signing_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Select signing type'),
      '#description' => $this->t('Choose signig type.'),
      '#required' => TRUE,
      '#weight' => 2,
    ];
    
    $signing_types = $this->efichajesManager->getSigningTypes();
    foreach ($signing_types as $value) {
      $form['field_ef_signing_type_id']['#options'][$value->id()] = 
      $value->getTitle();
    }
    
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 3,
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    $form['signings_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => $this->t('Today signings'),
      '#weight' => 4,
    ];
    
    $form['signings_table'] = [
      '#type' => 'table',
      '#header' => [
        'signing_type' => $this->t('Signing Type'),
        'hour' => $this->t('Hour'),
      ],
      '#empty' => $this->t('No signings found.'),
      '#weight' => 5,
    ];
    
    $signings = $this->efichajesManager->getSignings($user,
        $date_start->getTimestamp(), $date_end->getTimestamp());
    $signings_formatter = $this->efichajesFormatter->getSigningsDayResumeTable($signings);
    foreach ($signings_formatter as $key => $value) {
      $form['signings_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $date = new \DateTime();
    $request = $this->requestStack->getCurrentRequest();
    $server = $request->server;
    $parameters = $server->all();
    $ip = $parameters['HTTP_X_FORWARDED_FOR'];
    $data = $parameters['HTTP_USER_AGENT'];   
    $user = $this->efichajesManager->getUserById($this->current_user->id());
    $calendar_id = $user->get('field_ef_calendar_id')->getValue()[0]['target_id'];
    
    $params = [
      'title' => $this->t('Signing - @user - @signing_type - @date', [
        '@user' => $user->id(),
        '@signing_type' => $form_state->getValue('field_ef_signing_type_id'),
        '@date' => $date->format('d/m/Y H:i:s'),
      ]),
      'field_ef_calendar_id' => $calendar_id,
      'field_ef_data' => $data,
      'field_ef_date' => $date->getTimestamp(),
      'field_ef_ip' => $ip,
      'field_ef_signing_type_id' => $form_state->getValue('field_ef_signing_type_id'),
      'field_ef_user_id' => $user->id(),
      'status' => TRUE,
    ];
    
    $signing = $this->efichajesManager->addSigning($params);
    $this->messenger->addStatus($this->t('New Signing created.'));
  }
}
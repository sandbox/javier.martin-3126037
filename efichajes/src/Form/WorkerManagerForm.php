<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Component\Utility\EmailValidatorInterface;

class WorkerManagerForm extends FormBase {
  protected $efichajesManager;
  protected $messenger;
  protected $emailValidator;
  protected $worker;
  
  /**
   * Implementation construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger, EmailValidatorInterface $emailValidator) {
    $this->efichajesManager = $efichajesManager;
    $this->messenger = $messenger;
    $this->emailValidator = $emailValidator;
  }
  
  /**
   * Implementation create method.
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::create()
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('efichajes.manager'),
        $container->get('messenger'),
        $container->get('email.validator')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'WorkerManagerForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state){
    $mail = $form_state->getValue('mail');
    
    if (!$this->emailValidator->isValid($mail)) {
      $form_state->setErrorByName('mail', $this->t('Email address is not correct.'));
    }
    
    if (!isset($this->worker) && (null !== $this->efichajesManager->getUserByLogin($mail))) {
      $form_state->setErrorByName('username', $this->t('This user exists. Please, change email address.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $worker = null) {
    $this->worker = $worker;
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to add a worker.'),
    ];
    
    $form['field_ef_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Worker name.'),
      '#default_value' => (isset($worker)) ? $worker->get('field_ef_name')->value : '',
      '#required' => TRUE,
    ];
    
    $form['field_ef_surname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Surname'),
      '#description' => $this->t('Worker surname.'),
      '#default_value' => (isset($worker)) ? $worker->get('field_ef_surname')->value : '',
      '#required' => TRUE,
    ];
    
    $form['field_ef_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Id'),
      '#description' => $this->t('Worker id.'),
      '#default_value' => (isset($worker)) ? $worker->get('field_ef_id')->value : '',
    ];
    
    $form['field_ef_phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#description' => $this->t('Worker phone number.'),
      '#default_value' => (isset($worker)) ? $worker->get('field_ef_phone')->value : '',
    ];
    
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Mail'),
      '#description' => $this->t('Worker mail. Remember: Worker mail will be username.'),
      '#required' => TRUE,
      '#disabled' => (isset($worker)) ? TRUE : FALSE,
      '#default_value' => (isset($worker)) ? $worker->getEmail() : '',
    ];
    
    $form['pass'] = [
      '#type' => 'password_confirm',
      '#required' => (isset($worker)) ? FALSE : TRUE,
    ];
    
    $form['field_ef_calendar_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Canlendar'),
      '#description' => $this->t('Select calendar from availables.'),
      '#default_value' => (isset($worker)) ? $worker->get('field_ef_calendar_id')->getValue()[0]['target_id'] : '',
      '#required' => TRUE,
    ];
    
    $calendars = $this->efichajesManager->getCalendars(TRUE);
    foreach ($calendars as $value) {
      $form['field_ef_calendar_id']['#options'][$value->id()] = $value->getTitle();
    }
    
    $form['check_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('eFichajes Admin'),
      '#description' => $this->t('Check this box to assign eFichajes admin role.'),
      '#default_value' => (isset($worker)) ? $worker->hasRole('efichajes_admin') : FALSE,
    ];
    
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Check this box to enable worker.'),
      '#default_value' => (isset($worker)) ? $worker->isActive() : TRUE,
    ];
    
    $form['actions'] = [
      '#type' => 'actions'
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $params = [
      'field_ef_id' => $form_state->getValue('field_ef_id'),
      'field_ef_name' => $form_state->getValue('field_ef_name'),
      'field_ef_surname' => $form_state->getValue('field_ef_surname'),
      'field_ef_phone' => $form_state->getValue('field_ef_phone'),
      'field_ef_calendar_id' => $form_state->getValue('field_ef_calendar_id'),
      'efichajes_admin' => $form_state->getValue('check_admin'),
      'status' => $form_state->getValue('status'),
    ];
    
    if (!isset($this->worker)) {
      $params['name'] = $form_state->getValue('mail');
      $params['mail'] = $form_state->getValue('mail');
      $params['pass'] = $form_state->getValue('pass');
      $worker = $this->efichajesManager->addWorker($params);
      
      $this->messenger->addStatus($this->t('Added worker @surname, @name.', [
        '@surname' => $worker->get('field_ef_surname')->value,
        '@name' => $worker->get('field_ef_name')->value,
      ]));
    } else {
      if ($form_state->getValue('pass') != '')
        $params['pass'] = $form_state->getValue('pass');
      $this->efichajesManager->alterWorker($this->worker, $params);
      
      $this->messenger->addStatus($this->t('Altered worker @surname, @name.', [
        '@surname' => $this->worker->get('field_ef_surname')->value,
        '@name' => $this->worker->get('field_ef_name')->value,
      ]));
    }
    
    $form_state->setRedirect('efichajes.list.worker');
  }
}
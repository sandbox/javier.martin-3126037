<?php
namespace  Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

class CalendarManagerForm extends FormBase {
  protected $calendar;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implementation construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger) {
    $this->efichajesManager = $efichajesManager;
    $this->messenger = $messenger;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\CalendarManagerForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'CalendarManagerForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('status') && 
        $this->efichajesManager->hasCalendarWorkers($this->calendar)) {
      $form_state->setErrorByName('worker', $this->t('Sorry, you can not
        disable this work calendar because it has workers assigned.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $calendar = null) {
    $this->calendar = $calendar;
    $form['form_description'] = [
      '#markup' => 'Use this form to add/alter a calendar.',
    ];
    
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Intro calendar name.'),
      '#default_value' => (isset($calendar)) ? $calendar->getTitle() : '',
    ];
    
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Check this box to enable this calendar.'),
      '#default_value' => (isset($calendar)) ? $calendar->isPublished() : FALSE,
    ];
    
    $form['actions'] = [
      '#type' => 'actions'
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $params = [
      'title' => $form_state->getValue('title'),
      'status' => $form_state->getValue('status'),
    ];
    
    if (isset($this->calendar)) {
      $this->efichajesManager->alterCalendar($this->calendar, $params);
      $this->messenger->addStatus($this->t('Updated work calendar @title', [
        '@title' => $this->calendar->getTitle(),
      ]));
    } else {
      $calendar = $this->efichajesManager->addCalendar($params);
      $this->messenger->addStatus($this->t('Added work calendar @title', [
        '@title' => $calendar->getTitle(),
      ]));
    }
    
    $form_state->setRedirect('efichajes.list.calendar');
  }
}
<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Messenger\MessengerInterface;

class SigningTypeDeleteForm extends ConfirmFormBase {
  protected $signingtype;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implement construct method.
   * @param EfichajesManagerInterface $efichajesManager
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger) {
    $this->efichajesManager = $efichajesManager;
    $this->messenger = $messenger;
  }
  
  /**
   * Implement create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\SigningTypeDeleteForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }

  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure do you want to delete this Signing Type?');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'SigningTypeDeleteForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getConfirmText()
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getCancelText()
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $signings_count = $this->efichajesManager->hasSigningTypeSignings($this->signingtype);
    if ($signings_count != 0) {
      $form_state->setErrorByName('Signing Type', $this->t('Sorry, there are 
        signings assigned to this signing type. You can only disable this
        signing type, but not delete it.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $signingtype = null) {
    $this->signingtype = $signingtype;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title = $this->signingtype->getTitle();
    $this->efichajesManager->deleteNode($this->signingtype);
    
    $this->messenger($this->t('Deleted @title signing type.', [
      '@title' => $title]));
    
    $form_state->setRedirect('efichajes.list.signingtype');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.list.signingtype');
  }
}
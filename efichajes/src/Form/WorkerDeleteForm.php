<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\efichajes\EfichajesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\UserInterface;

class WorkerDeleteForm extends ConfirmFormBase {
  protected $worker;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implementation construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger) {
        $this->efichajesManager = $efichajesManager;
        $this->messenger = $messenger;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\WorkerDeleteForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->efichajesManager->hasWorkerSignings($this->worker)) {
      $form_state->setErrorByName('signings', $this->t('Sorry, you can not
        delete this worker, because he has signing assigned. You can only
        block him.'));
    }
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $worker = null) {
    $this->worker = $worker;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this worker?');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'WorkerDeleteForm';
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $this->worker->get('field_ef_surname')->value . ', ' .
        $this->worker->get('field_ef_name')->value;
    $this->efichajesManager->deleteUser($this->worker);
    $this->messenger->addStatus($this->t('Deleted worker @name', [
      '@name' => $name,
    ]));
    
    $form_state->setRedirect('efichajes.list.worker');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.list.worker');
  }
}
<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\efichajes\EfichajesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

class DateRangeManagerForm extends FormBase {
  protected $calendar;
  protected $daterange;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implementation construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger) {
        $this->efichajesManager = $efichajesManager;
        $this->messenger = $messenger;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\DateRangeManagerForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DateRangeManagerForm';
  }
  
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $date_start = new \DateTime($form_state->getValue('field_ef_date_start'));
    $date_end = new \DateTime($form_state->getValue('field_ef_date_end'));
    $priority = $form_state->getValue('field_ef_priority');
    
    if ($form_state->getValue('status') && 
        $this->efichajesManager->hasDateRangeCollision($this->calendar, 
            $this->daterange, $date_start->getTimestamp(), $date_end->getTimestamp(), $priority)) {
      $form_state->setErrorByName('collision', $this->t('This date range has 
      collision with other on same calendar. Please change priority or start/end date.'));
    }
  }
  
  /**
   * Return default values for work days checkbox.
   * @return mixed[]
   */
  protected function getWorkDaysDefaultValues() {
    $workdays = [];
    foreach ($this->daterange->get('field_ef_work_days')->getValue() as $key => $value) {
      if ($value['value'] != 0) {
        $workdays[$value['value']] = $value['value'];
      }
    }
    
    return $workdays;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $calendar = null, NodeInterface $daterange = null) {
    $this->calendar = $calendar;
    $this->daterange = $daterange;
    
    if (isset($daterange)) {
      $start_date = new \DateTime();
      $start_date->setTimestamp($daterange->get('field_ef_date_start')->value);
      
      $end_date = new \DateTime();
      $end_date->setTimestamp($daterange->get('field_ef_date_end')->value);
      
      $work_time = ($daterange->get('field_ef_work_time')->value / 60) / 60;
    }
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage ranges date.'),
    ];
    
    $form['field_ef_date_start'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#description' => $this->t('Intro range start date.'),
      '#required' => TRUE,
      '#default_value' => (isset($daterange)) ? $start_date->format('Y-m-d') : '',
    ];
    
    $form['field_ef_date_end'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#description' => $this->t('Intro range end date'),
      '#required' => TRUE,
      '#default_value' => (isset($daterange)) ? $end_date->format('Y-m-d') : '',
    ];
    
    $form['field_ef_work_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Workable hours'),
      '#description' => $this->t('Intro workable time per day in hours with two decimals if you need.'),
      '#step' => '.5',
      '#min' => 0,
      '#max' => 24,
      '#required' => TRUE,
      '#default_value' => (isset($daterange)) ? $work_time : 0,
    ];
    
    $form['field_ef_work_days'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Week Days'),
      '#description' => $this->t('Check week days when workable hours will take effect.'),
      '#options' => [
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wendsday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
        '7' => 'Sunday',
      ],
      '#default_value' => (isset($this->daterange)) ? $this->getWorkDaysDefaultValues() : [],
    ];
    
    $form['field_ef_priority'] = [
      '#type' => 'number',
      '#title' => $this->t('Priority'),
      '#description' => $this->t('Fill with range date priority over other range date with same period.'),
      '#min' => 1,
      '#max' => 10,
      '#step' => 1,
      '#default_value' => (isset($daterange)) ? $daterange->get('field_ef_priority')->value : 1,
    ];
    
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Check this box to enable range date.'),
      '#default_value' => (isset($daterange)) ? $daterange->isPublished() : FALSE,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $start_date = \DateTime::createFromFormat('Y-m-d H:i:s', $form_state->getValue('field_ef_date_start') . ' 23:59:59');
    $end_date = \DateTime::createFromFormat('Y-m-d H:i:s', $form_state->getValue('field_ef_date_end') . ' 00:00:00');
    $params = [
      'title' => $this->t('Range date @start_date to @end_date by @hours hours, @workable.', [
        '@start_date' => $start_date->format('d/m/Y'),
        '@end_date' => $end_date->format('d/m/Y'),
        '@hours' => $form_state->getValue('field_ef_workable_hours'),
        '@workable' => ($form_state->getValue('field_ef_workable_hours') != 0) ? $this->t('Workable') : $this->t('No Workable'),
      ]),
      'field_ef_date_start' => $start_date->getTimestamp(),
      'field_ef_date_end' => $end_date->getTimestamp(),
      'field_ef_work_time' => $form_state->getValue('field_ef_work_time') * 60 * 60,
      'field_ef_work_days' => $form_state->getValue('field_ef_work_days'),
      'field_ef_priority' => $form_state->getValue('field_ef_priority'),
      'status' => $form_state->getValue('status'),
    ];
    if (isset($this->daterange)) {
      $this->efichajesManager->alterDateRange($this->daterange, $params);
      
      $this->messenger->addStatus($this->t('Altered range date @start_date to @end_date by @hours hours, @workable.', [
        '@start_date' => $start_date->format('d/m/Y'),
        '@end_date' => $end_date->format('d/m/Y'),
        '@hours' => $form_state->getValue('field_ef_work_time'),
        '@workable' => ($form_state->getValue('field_ef_work_time') != 0) ? $this->t('Workable') : $this->t('No Workable'),
      ]));
    } else {
      $daterange = $this->efichajesManager->addDateRange($this->calendar, $params);
      
      $this->messenger->addStatus($this->t('Added range date @start_date to @end_date by @hours hours, @workable.', [
        '@start_date' => $start_date->format('d/m/Y'),
        '@end_date' => $end_date->format('d/m/Y'),
        '@hours' => $form_state->getValue('work_time'),
        '@workable' => ($form_state->getValue('work_time') != 0) ? $this->t('Workable') : $this->t('No Workable'),
      ]));
    }
    
    $form_state->setRedirect('efichajes.list.daterange', [
      'calendar' => $this->calendar->id(),
    ]);
  }
}
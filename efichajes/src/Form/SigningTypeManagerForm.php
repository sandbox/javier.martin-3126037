<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

class SigningTypeManagerForm extends FormBase {
  protected $signingtype;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implement construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager, 
      MessengerInterface $messenger) {
    $this->efichajesManager = $efichajesManager;
    $this->messenger = $messenger;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::create()
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'SigningTypeManagerForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $signingtype = null) {
    $this->signingtype = $signingtype;
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to add/alter a signing type.')
    ];
    
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signing Type'),
      '#description' => $this->t('Intro signing type.'),
      '#default_value' => (isset($signingtype)) ? $signingtype->getTitle() : '',
    ];
    
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => (isset($signingtype)) ? $signingtype->isPublished() : '',
      '#description' => $this->t('Check this box to enabled signing type.'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions'
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = [
      'title' => $form_state->getValue('title'),
      'status' => $form_state->getValue('enabled'),
    ];
    
    if (isset($this->signingtype)) {
      $this->efichajesManager->alterSigningType($this->signingtype, $values);
      $this->messenger->addStatus($this->t('Alter signing type @title', [
        '@title' => $this->signingtype->getTitle(),
      ]));
    } else {
      $signingtype = $this->efichajesManager->addSigningType($values);
      $this->messenger->addStatus($this->t('Added new signing type @title', [
        '@title' => $signingtype->getTitle(),
      ]));
    }
    
    $form_state->setRedirect('efichajes.list.signingtype');
  }
}
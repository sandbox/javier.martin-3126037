<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\efichajes\EfichajesFormatterInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class WorkerDashboardForm extends FormBase {
  protected $efichajesManager;
  protected $efichajesFormatter;
  protected $current_user;
  
  /**
   * Construct implementation
   * @param EfichajesManagerInterface $efichajesManager
   * @param EfichajesFormatterInterface $efichajesFormatter
   * @param AccountProxyInterface $current_user
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      EfichajesFormatterInterface $efichajesFormatter, 
      AccountProxyInterface $current_user) {
    $this->efichajesManager = $efichajesManager;
    $this->efichajesFormatter = $efichajesFormatter;
    $this->current_user = $current_user;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\WorkerDashboardForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter'),
        $container->get('current_user'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'WorkerDashboardForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $worker = $this->efichajesManager->getUserById($this->current_user->id());
    
    $form['form_description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Welcome to your dashboard.'),
    ];
    
    $form['daysignig_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => $this->t('Day signings'),
    ];
    
    $form['daysigning_table'] = [
      '#type' => 'table',
      '#header' => [
        'signing_type' => $this->t('Signing Type'),
        'hour' => $this->t('Hour'),
      ],
      '#empty' => $this->t('No signings found.'),
    ];
    
    $date_start = new \DateTime(); $date_start->setTime(0, 0, 0);
    $date_end = new \DateTime(); $date_end->setTime(23,59,0);
    $signings = $this->efichajesManager->getSignings($worker,
        $date_start->getTimestamp(), $date_end->getTimestamp());
    $signings_formatter = $this->efichajesFormatter->getSigningsDayResumeTable($signings);
    foreach ($signings_formatter as $key => $value) {
      $form['daysigning_table'][$key] = $value;
    }
    
    $form['montsigning_title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => $this->t('Month signings'),
    ];
    
    $form['monthsigning_table'] = [
      '#type' => 'table',
      '#header' => [
        'date' => $this->t('Date'),
        'signings' => $this->t('Signings'),
        'worktime' => $this->t('Work Time'),
        'theoretical_worktime' => $this->t('Theoretical Work Time'),
        'balance_time' => $this->t('Day balance'),
      ],
      '#empty' => $this->t('No signings found.'),
    ];
    
    $date_start = new \DateTime('first day of this month'); $date_start->setTime(0, 0, 0);
    $date_end = new \DateTime(); $date_end->setTime(23,59,0);
    $table_rows = $this->efichajesFormatter->getSigningsListTable(
        $worker, $date_start->getTimestamp(), $date_end->getTimestamp());
    foreach ($table_rows as $key => $value) {
      $form['monthsigning_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}
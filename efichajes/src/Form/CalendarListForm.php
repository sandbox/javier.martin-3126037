<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\efichajes\EfichajesFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class CalendarListForm extends FormBase {
  protected $efichajesManager;
  protected $efichajesFormatter;
  
  /**
   * Implementation construct method
   * @param EfichajesManagerInterface $efichajesManager
   * @param EfichajesFormatterInterface $efichajesFormatter
   */
  public function __construct(EfichajesManagerInterface $efichajesManager, 
      EfichajesFormatterInterface $efichajesFormatter) {
    $this->efichajesManager = $efichajesManager;
    $this->efichajesFormatter = $efichajesFormatter;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\CalendarListForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'CalendarListForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage work calendars.'),
    ];
    
    $form['calendars_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('Id'),
        'title' => $this->t('Description'),
        'enabled' => $this->t('Enabled'),
        'actions' => $this->t('Actions'),
      ],
      '#empty' => $this->t('No calendars founds.'),
    ];
    
    $calendars = $this->efichajesManager->getCalendars();
    $calendars_formatter = $this->efichajesFormatter->getCalendarsTable($calendars);
    foreach ($calendars_formatter as $key => $value) {
      $form['calendars_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}
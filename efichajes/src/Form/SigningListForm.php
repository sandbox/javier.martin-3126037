<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\efichajes\EfichajesFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Session\AccountProxyInterface;

class SigningListForm extends FormBase {
  protected $efichajesManager;
  protected $efichajesFormatter;
  protected $current_user;
  protected $worker;
  
  /**
   * Construct implementation.
   * @param EfichajesManagerInterface $efichajesManager
   * @param EfichajesFormatterInterface $efichajesFormatter
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      EfichajesFormatterInterface $efichajesFormatter,
      AccountProxyInterface $current_user) {
    $this->efichajesManager = $efichajesManager;
    $this->efichajesFormatter = $efichajesFormatter;
    $this->current_user = $current_user;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\SigningListForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter'),
        $container->get('current_user'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'SigningListForm';
  }
  
  
  public function ajaxrefreshbutton(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#signings-table', render($form['signings_table'])));
    
    return $response;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $worker = null) {
    $this->worker = (isset($worker)) ? $worker : $this->efichajesManager->getUserById($this->current_user->id());
    
    $form['field_ef_date_start'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#description' => $this->t('Select start signings date.'),
      '#weight' => 1,
    ];
    
    $form['field_ef_date_end'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#description' => $this->t('Select end signings date.'),
      '#weight' => 2,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 3,
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Submit'),
      '#name' => 'submitbutton',
      '#ajax' => [
        'callback' => [$this, 'ajaxrefreshbutton'],
      ],
    ];
    
    $form['signings_table'] = [
      '#type' => 'table',
      '#header' => [
        'date' => $this->t('Date'),
        'signings' => $this->t('Signings'),
        'worktime' => $this->t('Work Time'),
        'theoretical_worktime' => $this->t('Theoretical Work Time'),
        'balance_time' => $this->t('Day balance'),
      ],
      '#empty' => $this->t('No signings found.'),
      '#attributes' => [
        'id' => 'signings-table',
      ],
      '#weight' => 4,
    ];
    
    $element = $form_state->getTriggeringElement();
    if (!empty($element)) {
      $date_start = new \DateTime($form_state->getValue('field_ef_date_start'));
      $date_end = new \DateTime($form_state->getValue('field_ef_date_end'));
      $table_rows = $this->efichajesFormatter->getSigningsListTable(
        $this->worker, $date_start->getTimestamp(), $date_end->getTimestamp());
      foreach ($table_rows as $key => $value) {
        $form['signings_table'][$key] = $value;
      }
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { 
  }
}
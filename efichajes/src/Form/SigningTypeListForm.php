<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\efichajes\EfichajesFormatterInterface;

class SigningTypeListForm extends FormBase {
  protected $efichajesManager;
  protected $efichajesFormatter;
  
  /**
   * Implementation contruct method.
   * @param EfichajesManagerInterface $efichajesManager
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      EfichajesFormatterInterface $efichajesFormatter) {
    $this->efichajesManager = $efichajesManager;
    $this->efichajesFormatter = $efichajesFormatter;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\SigningTypeListForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'SigningTypeListForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#markup' => $this->t('In this form you can find all signing types and you can manage them.')
    ];
    
    $form['signingtypes_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('Id'),
        'signingtype' => $this->t('Signing type'),
        'enabled' => $this->t('Enabled'),
        'actions' => $this->t('Actions'),
      ],
      '#empty' => $this->t('No signing types found.'),
    ];
    
    $signingtypes = $this->efichajesManager->getSigningTypes();
    $signingtypes_formatter = $this->efichajesFormatter->getSigningTypesTable($signingtypes);
    foreach ($signingtypes_formatter as $key => $value) {
      $form['signingtypes_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}
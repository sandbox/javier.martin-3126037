<?php
namespace  Drupal\efichajes\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DateRangeDeleteForm extends ConfirmFormBase {
  protected $daterange;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implement construct method.
   * @param EfichajesManagerInterface $efichajesManager
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger) {
        $this->efichajesManager = $efichajesManager;
        $this->messenger = $messenger;
  }
  
  /**
   * Implement create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\DateRangeDeleteForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure do you want to delete this range date?');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DateRangeDeleteForm';
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getConfirmText()
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getCancelText()
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $daterange = null) {
    $this->daterange = $daterange;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title = $this->daterange->getTitle();
    $this->efichajesManager->deleteNode($this->daterange);
    
    $this->messenger($this->t('Deleted @title range date.', [
      '@title' => $title]));
    
    $form_state->setRedirect('efichajes.list.daterange', [
      'calendar' => $this->daterange->get('field_ef_calendar_id')->getValue()[0]['target_id'],
    ]);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.list.daterange', [
      'calendar' => $this->daterange->get('field_ef_calendar_id')->getValue()[0]['target_id'],
    ]);
  }
}
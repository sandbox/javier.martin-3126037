<?php
namespace  Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesFormatterInterface;
use Drupal\efichajes\EfichajesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

class DateRangeListForm extends FormBase {
  protected $calendar;
  protected $efichajesManager;
  protected $efichajesFormatter;
  
  /**
   * Implementation construct method
   * @param EfichajesManagerInterface $efichajesManager
   * @param EfichajesFormatterInterface $efichajesFormatter
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      EfichajesFormatterInterface $efichajesFormatter) {
        $this->efichajesManager = $efichajesManager;
        $this->efichajesFormatter = $efichajesFormatter;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\DateRangeListForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'DateRangeListForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $calendar = null) {
    $this->calendar = $calendar;
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage ranges date for a work calendar.'),
    ];
    
    $form['datesrange_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('Id'),
        'date_start' => $this->t('Start date'),
        'date_end' => $this->t('End date'),
        'priority' => $this->t('Priority'),
        'work_time' => $this->t('Hours'),
        'workable' => $this->t('Workable'),
        'status' => $this->t('Enabled'),
        'actions' => $this->t('Actions'),
      ],
      '#empty' => $this->t('No ranges date found for this work calendar.'),
    ];
    
    $datesrange = $this->efichajesManager->getDateRanges($calendar);
    $datesrange_formatter = $this->efichajesFormatter->getDatesRangeTable($datesrange);
    foreach ($datesrange_formatter as $key => $value) {
      $form['datesrange_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
  
}
<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

class Login extends FormBase {
  protected $efichajesManager;
  protected $current_user;
  protected $logger;
  
  /**
   * Construct implementation.
   * @param EfichajesManagerInterface $efichajesManager
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      LoggerChannelFactoryInterface $logger, 
      AccountProxyInterface $current_user) {
    $this->efichajesManager = $efichajesManager;
    $this->current_user = $current_user;
    $this->logger = $logger->get('efichajes');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\Login
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('logger.factory'),
        $container->get('current_user')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'LoginForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    $pass = $form_state->getValue('pass');
    $user = $this->efichajesManager->getUserByLogin($name);
    if (isset($user)) {
      if (!$user->hasRole('efichajes_worker')) {
        $form_state->setErrorByName('Role', $this->t('Sorry, you have not right role.'));
      }
      if (!$this->efichajesManager->isPasswordValid($pass, $user->getPassword())) {
        $form_state->setErrorByName('Pass', $this->t('Password incorrect.'));
      }
    } else {
      $form_state->setErrorByName('Username', $this->t('User not exists.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user = $this->efichajesManager->getUserById($this->current_user->id());
    
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Intro username.'),
    ];
    
    $form['pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Intro your password.'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    if ($user->hasRole('efichajes_worker')) {
      $url = new Url('efichajes.worker.signing');
      return new RedirectResponse($url->toString());
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    $user = $this->efichajesManager->getUserByLogin($name);
    
    user_login_finalize($user);
    
    $this->logger->info('@current_user - New login by @name', [
      '@current_user' => $this->current_user->getAccountName(),
      '@name' => $user->getUsername(),
    ]);
    
    $form_state->setRedirect('efichajes.worker.signing');
  }
}
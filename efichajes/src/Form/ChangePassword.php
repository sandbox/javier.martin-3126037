<?php
namespace  Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\efichajes\EfichajesManagerInterface;

class ChangePassword extends FormBase {
  protected $efichajesManager;
  protected $current_user;
  protected $messenger;
  
  /**
   * Construct implementation.
   * @param EfichajesManagerInterface $efichajesManager
   * @param AccountProxyInterface $current_user
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      AccountProxyInterface $current_user,
      MessengerInterface $messenger) {
    $this->efichajesManager = $efichajesManager;
    $this->current_user = $current_user;
    $this->messenger = $messenger;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ChangePassword
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('current_user'),
        $container->get('messenger'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'ChangePasswordForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Use this form to change your password.'),
    ];
    
    $form['pass'] = [
      '#type' => 'password_confirm',
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $worker = $this->efichajesManager->getUserById($this->current_user->id());
    if (isset($worker)) {
      $params = [
        'pass' => $form_state->getValue('pass'),
      ];
      
      $this->efichajesManager->alterWorker($worker, $params);
      $this->messenger->addStatus($this->t('Updated user password.'));
    } else {
      $this->messenger->addError($this->t('User not found.'));
    }
    
    $form_state->setRedirect('efichajes.worker.signing');
  }
}
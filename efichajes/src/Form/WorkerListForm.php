<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\efichajes\EfichajesFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class WorkerListForm extends FormBase {
  protected $efichajesManager;
  protected $efichajesFormatter;
  
  /**
   * Implement construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param EfichajesFormatterInterface $efichajesFormatter
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      EfichajesFormatterInterface $efichajesFormatter) {
    $this->efichajesManager = $efichajesManager;
    $this->efichajesFormatter = $efichajesFormatter;
  }
  
  /**
   * Implement create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\WorkerListForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('efichajes.formatter')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'WorkerListForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage all your business workers.'),
    ];
    
    $form['workers_table'] = [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('Id'),
        'name' => $this->t('Username'),
        'name_surname' => $this->t('Name'),
        'status' => $this->t('Status'),
        'actions' => $this->t('Actions'),
      ],
      '#empty' => $this->t('No workers found.')
    ];
    
    $workers = $this->efichajesManager->getWorkers();
    $workers_formatter = $this->efichajesFormatter->getWorkersTable($workers);
    foreach ($workers_formatter as $key => $value) {
      $form['workers_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}
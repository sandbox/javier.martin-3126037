<?php
namespace  Drupal\efichajes\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\efichajes\EfichajesManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CalendarDeleteForm extends ConfirmFormBase {
  protected $calendar;
  protected $efichajesManager;
  protected $messenger;
  
  /**
   * Implementation construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param MessengerInterface $messenger
   */
  public function __construct(EfichajesManagerInterface $efichajesManager,
      MessengerInterface $messenger) {
    $this->efichajesManager = $efichajesManager;
    $this->messenger = $messenger;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\CalendarDeleteForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('efichajes.manager'),
        $container->get('messenger')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->efichajesManager->hasCalendarWorkers($this->calendar)) {
      $form_state->setErrorByName('workers', $this->t('Sorry, you can delete 
        this calendar now. It has worker assigned.'));
    }
    
    if ($this->efichajesManager->hasCalendarDatesRange($this->calendar)) {
      $form_state->setErrorByName('RangeDate', $this->t('Sorry, you can delete 
        this calendar now. It has range date assigned.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $calendar = null) {
    $this->calendar = $calendar;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this calendar?');
  }

  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'CalendarDeleteForm';
  }

  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title = $this->calendar->getTitle();
    $this->efichajesManager->deleteNode($this->calendar);
    $this->messenger->addStatus($this->t('Delete Work Calendar @title.', [
      '@title' => $title,
    ]));
    
    $form_state->setRedirect('efichajes.list.calendar');
  }

  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.list.calendar');
  }
}
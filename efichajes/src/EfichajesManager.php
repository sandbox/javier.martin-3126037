<?php
namespace Drupal\efichajes;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

class EfichajesManager implements EfichajesManagerInterface {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $passwordHasher;
  
  /**
   * Implementation construct function.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   * @param PasswordInterface $passwordHasher
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager,
      LoggerChannelFactoryInterface $logger, 
      PasswordInterface $passwordHasher) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
    $this->passwordHasher = $passwordHasher;
  }
  
  /**
   * Implement create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\EfichajesManager
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('current_user'),
        $container->get('entity_type.manager'),
        $container->get('logger.factory'),
        $container->get('password')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::isPasswordValid()
   */
  public function isPasswordValid(String $password, String $cryptpassword) {
    return $this->passwordHasher->check($password, $cryptpassword);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getUserByLogin()
   */
  public function getUserByLogin(String $login) {
    $storage = $this->entityTypeManager->getStorage('user');
    $users = $storage->loadByProperties([
      'name' => $login,
    ]);
    
    return empty($users) ? null : array_shift($users);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getSigningTypes()
   */
  public function getSigningTypes(int $status = 1) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signingtypes = $storage->loadByProperties([
      'type' => 'signing_type',
      'status' => $status,
    ]);
    
    return $signingtypes;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::addSigningType()
   */
  public function addSigningType(array $params) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signingtype = $storage->create([
      'type' => 'signing_type',
      'title' => $params['title'],
      'status' => $params['status'],
    ]);
    $signingtype->save();
    
    $this->logger->info('@current_user - Added signing type @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $signingtype->getTitle(),
    ]);
    
    return $signingtype;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::alterSigningType()
   */
  public function alterSigningType(NodeInterface $signingtype, array $params) {
    if (array_key_exists('title', $params))
      $signingtype->get('title')->setValue($params['title']);
    if (array_key_exists('status', $params))
      $signingtype->get('status')->setValue($params['status']);
    $signingtype->save();
    
    $this->logger->info('@current_user - Alter signing type @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $signingtype->getTitle(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::deleteNode()
   */
  public function deleteNode(NodeInterface $node) {
    $title = $node->getTitle();
    $type = $node->getType();
    $node->delete();
    
    $this->logger->info('@current_user - Delete @type @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@type' => $type,
      '@title' => $title,
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getSignings()
   */
  public function hasSigningTypeSignings(NodeInterface $signingtype) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signingtype_count = $storage->getQuery()
    ->condition('type', 'signing')
    ->condition('field_ef_signing_type_id', $signingtype->id())
    ->count()
    ->execute();
    
    return ($signingtype_count != 0) ? TRUE : FALSE;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getWorkers()
   */
  public function getWorkers() {
    $storage = $this->entityTypeManager->getStorage('user');
    $workers = $storage->loadByProperties([
      'roles' => 'efichajes_worker',
    ]);
    
    return $workers;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::addWorker()
   */
  public function addWorker(array $params) {
    $storage = $this->entityTypeManager->getStorage('user');
    $worker = $storage->create([
      'name' => $params['name'],
      'mail' => $params['mail'],
      'pass' => $params['pass'],
      'field_ef_id' => $params['field_ef_id'],
      'field_ef_name' => $params['field_ef_name'],
      'field_ef_surname' => $params['field_ef_surname'],
      'field_ef_phone' => $params['field_ef_phone'],
      'field_ef_calendar_id' => $params['field_ef_calendar_id'],
      'roles' => ['efichajes_worker'],
      'status' => $params['status'],
    ]);
    if ($params['efichajes_admin']) {
      $worker->addRole('efichajes_admin');
    }
    $worker->save();
    
    $this->logger->info('@current_user - Add worker @name', [
      '@current_user' => $this->current_user->getAccountName(),
      '@name' => $worker->getAccountName(), 
    ]);
    
    return $worker;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::alterWorker()
   */
  public function alterWorker(UserInterface $worker, array $params) {
    if (array_key_exists('pass', $params)) {
      $worker->setPassword($params['pass']);
    }
    if (array_key_exists('field_ef_id', $params)) {
      $worker->get('field_ef_id')->setValue($params['field_ef_id']);
    }
    if (array_key_exists('field_ef_name', $params)) {
      $worker->get('field_ef_name')->setValue($params['field_ef_name']);
    }
    if (array_key_exists('field_ef_surname', $params)) {
      $worker->get('field_ef_surname')->setValue($params['field_ef_surname']);
    }
    if (array_key_exists('field_ef_phone', $params)) {
      $worker->get('field_ef_phone')->setValue($params['field_ef_phone']);
    }
    if (array_key_exists('field_ef_calendar_id', $params)) {
      $worker->get('field_ef_calendar_id')->setValue($params['field_ef_calendar_id']);
    }
    if (array_key_exists('efichajes_admin', $params)) {
      if ($params['efichajes_admin']) {
        $worker->addRole('efichajes_admin');
      } else {
        $worker->removeRole('efichajes_admin');
      }
    }
    if (array_key_exists('status', $params)) {
      if ($params['status']) {
        $worker->activate();
      } else {
        $worker->get('status')->setValue(FALSE);
      }
    }
    $worker->save();
    
    $this->logger->info('@current_user - Altered worker @name', [
      '@current_user' => $this->current_user->getAccountName(),
      '@name' => $worker->getAccountName(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::deleteUser()
   */
  public function deleteUser(UserInterface $worker) {
    $name = $worker->getAccountName();
    $worker->delete();
    
    $this->logger->info('@current_user - Add worker @name', [
      '@current_user' => $this->current_user->getAccountName(),
      '@name' => $worker->getAccountName(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::hasWorkerSignings()
   */
  public function hasWorkerSignings(UserInterface $worker) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signing_count = $storage->getQuery()
    ->condition('type', 'signing')
    ->condition('field_ef_user_id', $worker->id())
    ->count()
    ->execute();
    
    return ($signing_count != 0) ? TRUE : FALSE;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getCalendars()
   */
  public function getCalendars(int $status = 1) {
    $store = $this->entityTypeManager->getStorage('node');
    $calendars = $store->loadByProperties([
      'type' => 'calendar',
      'status' => $status,
    ]);
    
    return $calendars;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::addCalendar()
   */
  public function addCalendar(array $params) {
    $storage = $this->entityTypeManager->getStorage('node');
    $calendar = $storage->create([
      'type' => 'calendar',
      'title' => $params['title'],
      'status' => TRUE,
    ]);
    $calendar->save();
    
    $this->logger->info('@current_user - Add work calendar @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $calendar->getTitle(),
    ]);
    
    return $calendar;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::alterCalendar()
   */
  public function alterCalendar(NodeInterface $calendar, array $params) {
    if (array_key_exists('title', $params)) {
      $calendar->setTitle($params['title']);
    }
    if (array_key_exists('status', $params)) {
      $calendar->setPublished($params['status']);
    }
    $calendar->save();
    
    $this->logger->info('@current_user - Alter work calendar @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $calendar->getTitle(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::hasCalendarWorkers()
   */
  public function hasCalendarWorkers(NodeInterface $calendar) {
    $storage = $this->entityTypeManager->getStorage('user');
    $user_count = $storage->getQuery()
    ->condition('field_ef_calendar_id', $calendar->id())
    ->count()
    ->execute();
    
    return ($user_count != 0) ? TRUE : FALSE;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::hasCalendarDatesRange()
   */
  public function hasCalendarDatesRange(NodeInterface $calendar) {
    $storage = $this->entityTypeManager->getStorage('node');
    $calendar_count = $storage->getQuery()
    ->condition('type', 'date_range')
    ->condition('field_ef_calendar_id', $calendar->id())
    ->count()
    ->execute();
    
    return ($calendar_count != 0) ? TRUE : FALSE;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getDateRanges()
   */
  public function getDateRanges(NodeInterface $calendar) {
    $storage = $this->entityTypeManager->getStorage('node');
    $daterange = $storage->loadByProperties([
      'type' => 'date_range',
      'field_ef_calendar_id' => $calendar->id(),
    ]);
    
    return $daterange;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::addDateRange()
   */
  public function addDateRange(NodeInterface $calendar, array $params) {
    $storage = $this->entityTypeManager->getStorage('node');
    $daterange = $storage->create([
      'type' => 'date_range',
      'title' => $params['title'],
      'field_ef_calendar_id' => $calendar->id(),
      'field_ef_date_start' => $params['field_ef_date_start'],
      'field_ef_date_end' => $params['field_ef_date_end'],
      'field_ef_priority' => $params['field_ef_priority'],
      'field_ef_work_time' => $params['field_ef_work_time'],
      'field_ef_work_days' => $params['field_ef_work_days'],
      'status' => $params['status'],
    ]);
    $daterange->save();
    
    $this->logger->info('@current_user - Add range date @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $daterange->getTitle(),
    ]);
    
    return $daterange;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::alterDateRange()
   */
  public function alterDateRange(NodeInterface $daterange, array $params) {
    if (array_key_exists('title', $params)) {
      $daterange->setTitle($params['title']);
    }
    if (array_key_exists('field_ef_date_start', $params)) {
      $daterange->get('field_ef_date_start')->setValue($params['field_ef_date_start']);
    }
    if (array_key_exists('field_ef_date_end', $params)) {
      $daterange->get('field_ef_date_end')->setValue($params['field_ef_date_end']);
    }
    if (array_key_exists('field_ef_priority', $params)) {
      $daterange->get('field_ef_priority')->setValue($params['field_ef_priority']);
    }
    if (array_key_exists('field_ef_work_time', $params)) {
      $daterange->get('field_ef_work_time')->setValue($params['field_ef_work_time']);
    }
    if (array_key_exists('field_ef_work_days', $params)) {
      $daterange->get('field_ef_work_days')->setValue($params['field_ef_work_days']);
    }
    if (array_key_exists('status', $params)) {
      $daterange->get('status')->setValue($params['status']);
    }
    $daterange->save();
    
    $this->logger->info('@current_user - Alter range date @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $daterange->getTitle(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::hasDateRangeCollision()
   */
  public function hasDateRangeCollision(NodeInterface $calendar, 
      NodeInterface $daterange = null, int $date_start, 
      int $date_end, int $priority) {
    $storage = $this->entityTypeManager->getStorage('node');
    
    $query = $storage->getQuery()
      ->condition('type', 'date_range')
      ->condition('field_ef_calendar_id', $calendar->id())
      ->condition('field_ef_priority', $priority)
      ->condition('status', TRUE);
    
    if (isset($daterange)) {
      $query->condition('nid', $daterange->id(), '!=');
    }
    
    $query->orConditionGroup()
      ->condition('field_ef_date_start', [$date_start, $date_end], 'BETWEEN')
      ->condition('field_ef_date_end', [$date_start, $date_end], 'BETWEEN');
    
    $count = $query
      ->count()
      ->execute();
    
    return ($count != 0) ? TRUE : FALSE;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getUserById()
   */
  public function getUserById(int $id) {
    $storage = $this->entityTypeManager->getStorage('user');
    
    return $storage->load($id);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getSignings()
   */
  public function getSignings(UserInterface $worker, int $date_start, int $date_end) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signings = $storage->getQuery()
      ->condition('type', 'signing')
      ->condition('status', TRUE)
      ->condition('field_ef_user_id', $worker->id())
      ->condition('field_ef_date', [$date_start, $date_end], 'BETWEEN')
      ->execute();
    
    return $storage->loadMultiple($signings);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::addSigning()
   */
  public function addSigning(array $params) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signing = $storage->create([
      'type' => 'signing',
      'title' => $params['title'],
      'field_ef_calendar_id' => $params['field_ef_calendar_id'],
      'field_ef_data' => $params['field_ef_data'],
      'field_ef_date' => $params['field_ef_date'],
      'field_ef_ip' => $params['field_ef_ip'],
      'field_ef_signing_type_id' => $params['field_ef_signing_type_id'],
      'field_ef_user_id' => $params['field_ef_user_id'],
      'status' => $params['status'],
    ]);
    $signing->save();
    
    $this->logger->info('@current_user - Add signing @title', [
      '@current_user' => $this->current_user->getAccountName(),
      '@title' => $signing->getTitle(),
    ]);
    
    return $signing;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getNode()
   */
  public function getNode(int $id) {
    $storage = $this->entityTypeManager->getStorage('node');
    
    return $storage->load($id);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getWorkTime()
   */
  public function getWorkTimePerDay(array $signings) {
    $time = 0;
    $signing_values = array_values($signings);
    
    for($i = 0;;$i+=2) {
      if (isset($signing_values[$i]) && isset($signing_values[$i+1])) {
        $date_start = $signing_values[$i]->get('field_ef_date')->value;
        $date_end = $signing_values[$i+1]->get('field_ef_date')->value;
        $diff = $date_end - $date_start;
        $time = $time + $diff;
      } else {
        break;
      }
    }
    
    return $time;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getDateRangeForDay()
   */
  public function getDateRangeForDay(UserInterface $worker, int $date) {
    $storage = $this->entityTypeManager->getStorage('node');
    $dateranges_id = $storage->getQuery()
      ->condition('type', 'date_range')
      ->condition('status', TRUE)
      ->condition('field_ef_calendar_id', $worker->get('field_ef_calendar_id')->getValue()[0]['target_id'])
      ->condition('field_ef_date_start', $date, '<')
      ->condition('field_ef_date_end', $date, '>')
      ->sort('field_ef_priority', 'DESC')
      ->execute();
    $dataranges = $storage->loadMultiple($dateranges_id);
    
    return (empty($dataranges)) ? null : reset($dataranges);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getTotalWorkTime()
   */
  public function getWorkTimePerPeriod(UserInterface $worker, int $date_start, int $date_end) {
    $date_pointer = new \DateTime(); $date_pointer->setTimestamp($date_start); $date_pointer->setTime(0,0,0);
    $date_finish = new \DateTime(); $date_finish->setTimestamp($date_end); $date_finish->setTime(23,59,59);
    $total_worktime = 0;
    
    do {
      $date_endday = new \DateTime();
      $date_endday->setTimestamp($date_pointer->getTimestamp());
      $date_endday->setTime(23,59,59);
      
      $signings = $this->getSignings($worker, $date_pointer->getTimestamp(), 
          $date_endday->getTimestamp()); 
      $total_worktime += $this->getWorkTimePerDay($signings);
      
      $date_pointer->add(new \DateInterval('P1D'));
    } while ($date_pointer->getTimestamp() < $date_finish->getTimestamp());
    
    return $total_worktime;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getTheoreticalTimePerDay()
   */
  public function getTheoreticalTimePerDay(UserInterface $worker, int $unixtimestamp) {
    $worktime = 0;
    $date_pointer = new \DateTime(); $date_pointer->setTimestamp($unixtimestamp);
    $date_week = $date_pointer->format('N');
    $date_range = $this->getDateRangeForDay($worker, $unixtimestamp);
    if (isset($date_range)) {
      $workdays = [];
      foreach ($date_range->get('field_ef_work_days')->getValue() as $value) {
        $workdays[$value['value']] = $value['value'];
      }
      if (array_search($date_week, $workdays) == $date_week) {
        $worktime = $date_range->get('field_ef_work_time')->value;
      }
    }
    
    return $worktime;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesManagerInterface::getTheoreticalTimePerPeriod()
   */
  public function getTheoreticalTimePerPeriod(UserInterface $worker, int $date_start, int $date_end) {
    $date_pointer = new \DateTime(); $date_pointer->setTimestamp($date_start); $date_pointer->setTime(0,0,0);
    $date_finish = new \DateTime(); $date_finish->setTimestamp($date_end); $date_finish->setTime(23,59,59);
    $total_theoreticaltime = 0;
    
    do {
      $date_endday = new \DateTime();
      $date_endday->setTimestamp($date_pointer->getTimestamp());
      $date_endday->setTime(23,59,59);
      
      $date_range = $this->getDateRangeForDay($worker, $date_pointer->getTimestamp());
      $total_theoreticaltime += isset($date_range) ? 
        $this->getTheoreticalTimePerDay($worker, $date_pointer->getTimestamp()) : 0;
      $date_pointer->add(new \DateInterval('P1D'));
    } while ($date_pointer->getTimestamp() < $date_finish->getTimestamp());
    
    return $total_theoreticaltime;
  }
}
<?php
namespace Drupal\efichajes\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\efichajes\EfichajesManagerInterface;

/**
 * 
 * Provide a block with user name.
 * 
 * @Block(
 *   id = "efichajes_username_block",
 *   admin_label = @Translation("Efichajes Username Block")
 * )
 */
class UserNameBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $efichajesManager;
  protected $current_user;
  
  /**
   * Construct implementation.
   * @param array $configuration
   * @param String $plugin_id
   * @param String $plugin_definition
   * @param EfichajesManagerInterface $efichajesManager
   * @param AccountProxyInterface $current_user
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, 
      EfichajesManagerInterface $efichajesManager, 
      AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->efichajesManager = $efichajesManager;
    $this->current_user = $current_user;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Plugin\ContainerFactoryPluginInterface::create()
   */
  public static function create(ContainerInterface $container, array $configuration, 
      $plugin_id, $plugin_definition) {
    return new static (
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('efichajes.manager'),
        $container->get('current_user'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Block\BlockPluginInterface::build()
   */
  public function build() {
    $user = $this->efichajesManager->getUserById($this->current_user->id());
    $title = empty($user->get('field_ef_name')->value) ?
      $user->getAccountName() :
      $user->get('field_ef_surname')->value . ', ' . $user->get('field_ef_name')->value;
    
    $form['title'] = [
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
      '#markup' => $title,
    ];
    
    return $form;
  }
}
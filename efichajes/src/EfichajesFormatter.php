<?php
namespace Drupal\efichajes;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\UserInterface;

class EfichajesFormatter implements EfichajesFormatterInterface {
  protected $entityTypeManager;
  protected $efichajesManager;
  
  /**
   * Implement construct method.
   * @param EfichajesManagerInterface $efichajesManager
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, 
      EfichajesManagerInterface $efichajesManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->efichajesManager = $efichajesManager;
  }
  
  /**
   * Implement create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\EfichajesFormatter
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager'),
        $container->get('efichajes.manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getSigningsTypeTable()
   */
  public function getSigningTypesTable(array $signingtypes) {
    $table = [];
    foreach ($signingtypes as $key => $value) {
      $url_alter = Url::fromRoute('efichajes.alter.signingtype', [
        'operation' => 'alter',
        'signingtype' => $value->id(),
      ]);
      $link_alter = Link::fromTextAndUrl(t('Alter'), $url_alter);
      
      $url_delete = Url::fromRoute('efichajes.delete.signingtype', [
        'operation' => 'delete',
        'signingtype' => $value->id(),
      ]);
      $link_delete = Link::fromTextAndUrl(t('Delete'), $url_delete);
      
      $table[$key] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'signingtype' => [
          '#markup' => $value->getTitle(),
        ],
        'enabled' => [
          '#markup' => ($value->isPublished()) ? t('Yes') : t('No'),
        ],
        'actions' => [
          'alter' => $link_alter->toRenderable(),
          'space' => ['#markup' => ' '],
          'delete' => $link_delete->toRenderable(),
        ],
      ];
    }
    
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getWorkersTable()
   */
  public function getWorkersTable(array $workers) {
    $table = [];
    foreach ($workers as $key => $value) {
      $url_alter = Url::fromRoute('efichajes.alter.worker', [
        'operation' => 'alter',
        'worker' => $value->id(),
      ]);
      $link_alter = Link::fromTextAndUrl(t('Alter'), $url_alter);
      
      $url_delete = Url::fromRoute('efichajes.delete.worker', [
        'operation' => 'delete',
        'worker' => $value->id(),
      ]);
      $link_delete = Link::fromTextAndUrl(t('Delete'), $url_delete);
      
      $url_signing = Url::fromRoute('efichajes.list.worker.signing', [
        'operation' => 'list-signings',
        'worker' => $value->id(),
      ]);
      $link_signing = Link::fromTextAndUrl(t('View signings'), $url_signing);
      
      $table[$key] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'name' => [
          '#markup' => $value->getAccountName(),
        ],
        'name_surname' => [
          '#markup' => $value->get('field_ef_surname')->value . ', ' . 
            $value->get('field_ef_name')->value,
        ],
        'status' => [
          '#markup' => ($value->isActive()) ? t('Active') : t('Blocked'),
        ],
        'actions' => [
          'alter' => $link_alter->toRenderable(),
          'space' => ['#markup' => ' '],
          'delete' => $link_delete->toRenderable(),
          'space2' => ['#markup' => ' '],
          'signing' => $link_signing->toRenderable(),
        ],
      ];
    }
    
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getCalendarsTable()
   */
  public function getCalendarsTable(array $calendars) {
    $table = [];
    foreach ($calendars as $key => $value) {
      $url_alter = Url::fromRoute('efichajes.alter.calendar', [
        'operation' => 'alter',
        'calendar' => $value->id(),
      ]);
      $link_alter = Link::fromTextAndUrl(t('Alter'), $url_alter);
      
      $url_delete = Url::fromRoute('efichajes.delete.calendar', [
        'operation' => 'delete',
        'calendar' => $value->id(),
      ]);
      $link_delete = Link::fromTextAndUrl(t('Delete'), $url_delete);
      
      $url_daterange = Url::fromRoute('efichajes.list.daterange', [
        'operation' => 'list',
        'calendar' => $value->id(),
      ]);
      $link_daterange = Link::fromTextAndUrl(t('Manage date range'), $url_daterange);
      
      
      $table[$key] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'title' => [
          '#markup' => $value->getTitle(),
        ],
        'enabled' => [
          '#markup' => ($value->isPublished()) ? t('Yes') : t('No'),
        ],
        'actions' => [
          'alter' => $link_alter->toRenderable(),
          'space' => ['#markup' => ' '],
          'delete' => $link_delete->toRenderable(),
          'space2' => ['#markup' => ' '],
          'rangesdate' => $link_daterange->toRenderable(),
        ],
      ];
    }
    
    return $table;
  }
  
  /**
   * Return format date from a unix timestamp.
   * @param int $unixtimestamp
   * @return string
   */
  protected function timestampToDate(int $unixtimestamp) {
    $date = new \DateTime();
    $date->setTimestamp($unixtimestamp);
    
    return $date->format('d/m/Y');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getDatesRangeTable()
   */
  public function getDatesRangeTable(array $datesrange) {
    $table = [];
    foreach ($datesrange as $key => $value) {
      $url_alter = Url::fromRoute('efichajes.alter.daterange', [
        'operation' => 'alter',
        'calendar' => $value->get('field_ef_calendar_id')->getValue()[0]['target_id'],
        'daterange' => $value->id(),
      ]);
      $link_alter = Link::fromTextAndUrl(t('Alter'), $url_alter);
      
      $url_delete = Url::fromRoute('efichajes.delete.daterange', [
        'operation' => 'delete',
        'calendar' => $value->get('field_ef_calendar_id')->getValue()[0]['target_id'],
        'daterange' => $value->id(),
      ]);
      $link_delete = Link::fromTextAndUrl(t('Delete'), $url_delete);
      
      $table[$key] = [
        'id' => [
          '#markup' => $value->id(),
        ],
        'date_start' => [
          '#markup' => $this->timestampToDate($value->get('field_ef_date_start')->value),
        ],
        'date_end' => [
          '#markup' => $this->timestampToDate($value->get('field_ef_date_end')->value),
        ],
        'priority' => [
          '#markup' => $value->get('field_ef_priority')->value,
        ],
        'work_time' => [
          '#markup' => (($value->get('field_ef_work_time')->value / 60) / 60),
        ],
        'workable' => [
          '#markup' => ($value->get('field_ef_work_time')->value != 0) ? t('Yes') : t('No'),
        ],
        'status' => [
          '#markup' => ($value->isPublished()) ? t('Yes') : t('No'),
        ],
        'actions' => [
          'alter' => $link_alter->toRenderable(),
          'space' => ['#markup' => ' '],
          'delete' => $link_delete->toRenderable(),
        ],
      ];
    }
    
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getSigningsDayResumeTable()
   */
  public function getSigningsDayResumeTable(array $signings) {
    $table = [];
    foreach ($signings as $value) {
      $timestamp = $value->get('field_ef_date')->value;
      $hour = new \DateTime(); $hour->setTimestamp($timestamp);
      $signingtype_id = $value->get('field_ef_signing_type_id')->getValue()[0]['target_id'];
      $signing = $this->efichajesManager->getNode($signingtype_id);
      $table[$value->id()] = [
        'signing_type' => [
          '#markup' => $signing->getTitle(),
        ],
        'hour' => [
          '#markup' => $hour->format('H:i:s'),
        ],
      ];
    }
    
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getSigningsListTable()
   */
  public function getSigningsListTable(UserInterface $worker, int $date_start, int $date_end) {
    $table = [];
    $date_pointer = new \DateTime(); 
    $date_pointer->setTimestamp($date_start); 
    $date_pointer->setTime(0,0,0);
    
    $date_finish = new \DateTime(); 
    $date_finish->setTimestamp($date_end); 
    $date_finish->setTime(23,59,59);
    
    do {
      $date_endday = new \DateTime(); 
      $date_endday->setTimestamp($date_pointer->getTimestamp());
      $date_endday->setTime(23,59,59);
      $signings = $this->efichajesManager->getSignings($worker, 
          $date_pointer->getTimestamp(), $date_endday->getTimestamp());
      
      $signings_row = [];
      foreach ($signings as $key => $value) {
        $signingtype_id = $value->get('field_ef_signing_type_id')->getValue()[0]['target_id'];
        $signingtype = $this->efichajesManager->getNode($signingtype_id);
        
        $datetimestamp = $value->get('field_ef_date')->value;
        $date = new \DateTime(); $date->setTimestamp($datetimestamp);
        
        $signings_row[] = [
          '#prefix' => '<p>',
          '#suffix' => '</p>',
          '#markup' => $signingtype->getTitle() . ' - ' . $date->format('H:i:s'),
        ];
      }
      
      $worktime = $this->efichajesManager->getWorkTimePerDay($signings);
      $worktime_date = new \DateTime(null, new \DateTimeZone( '+0000' )); 
      $worktime_date->setTimestamp($worktime);
      
      $date_range = $this->efichajesManager->getDateRangeForDay($worker, $date_pointer->getTimestamp());
      $theoretical_time = isset($date_range) ?
        $this->efichajesManager->getTheoreticalTimePerDay($worker, $date_pointer->getTimestamp()) : 0;
      
      $balance_time = $theoretical_time - $worktime;
      
      $table[$date_pointer->getTimestamp()] = [
        'date' => [
          '#markup' => $date_pointer->format('d/m/Y'),
        ],
        'signings' => $signings_row,
        'worktime' => [
          '#markup' => gmdate('H:i:s', $worktime),
        ],
        'theoretical_time' => [
          '#markup' => gmdate('H:i:s', $theoretical_time),
        ],
        'balance_time' => [
          '#markup' => ($balance_time < 0) ? 
          '+' . gmdate('H:i:s', $balance_time * (-1)) : '-' . gmdate('H:i:s', $balance_time),
        ],
      ];
      
      $date_pointer->add(new \DateInterval('P1D'));
    } while ($date_pointer->getTimestamp() < $date_finish->getTimestamp());
    
    $total_worktime = $this->efichajesManager->getWorkTimePerPeriod($worker, $date_start, $date_end);
    $total_theoreticaltime = $this->efichajesManager->getTheoreticalTimePerPeriod($worker, $date_start, $date_end);
    $balance_time = $total_theoreticaltime - $total_worktime;
    
    $table['total'] = [
      'date' => [
        '#markup' => '',
      ],
      'signings' => [
        '#markup' => '<strong>Total</strong>',
      ],
      'worktime' => [
        '#markup' => $this->getTimeString($total_worktime),
      ],
      'theoretical_time' => [
        '#markup' => $this->getTimeString($total_theoreticaltime),
      ],
      'balance_time' => [
        '#markup' => ($balance_time < 0) ?
        '+' . $this->getTimeString($balance_time * (-1)) : '-' . $this->getTimeString($balance_time),
      ],
    ];
    
    return $table;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesFormatterInterface::getTimeString()
   */
  public function getTimeString(int $timestamp) {
    $hours = strval(floor($timestamp / 3600));
    $minutes = strval(floor(($timestamp / 60) % 60));
    $seconds = strval($timestamp % 60);
    
    return t('@hours:@minutes:@seconds', [
      '@hours' => str_pad($hours, 2, '0', STR_PAD_LEFT),
      '@minutes' => str_pad($minutes, 2, '0', STR_PAD_LEFT),
      '@seconds' => str_pad($seconds, 2, '0', STR_PAD_LEFT),
    ]);
  }

}
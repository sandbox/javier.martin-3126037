<?php
namespace Drupal\efichajes;

use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

interface EfichajesManagerInterface {
  
  /**
   * Return user entity for a login.
   * @param String $login
   * @return UserInterface|NULL
   */
  public function getUserByLogin(String $login);
  
  /**
   * Return true if password is cryptpassword.
   * @param String $password
   * @param String $cryptpassword
   * @return bool
   */
  public function isPasswordValid(String $password, String $cryptpassword);
  
  /**
   * Return signing types.
   * @param int $status
   * @return array if no signings type are found, return an empty array
   */
  public function getSigningTypes(int $status = 1);
  
  /**
   * Add signing type.
   * @param array $params
   * @return NodeInterface
   */
  public function addSigningType(array $params);
  
  /**
   * Alter signing type.
   * @param NodeInterface $signingtype
   * @param array $params
   */
  public function alterSigningType(NodeInterface $signingtype, array $params);
  
  /**
   * Delete signing type.
   * @param NodeInterface $signingtype
   */
  public function deleteNode(NodeInterface $node);
  
  /**
   * Return true if there are signings with this signing type.
   * @param NodeInterface $signingtype
   * @return bool
   */
  public function hasSigningTypeSignings(NodeInterface $signingtype);
  
  /**
   * Return all workers.
   * @return array Return an empty array if no workers are founds.
   */
  public function getWorkers();
  
  /**
   * Add worker.
   * @param array $params
   * @return UserInterface
   */
  public function addWorker(array $params);
  
  /**
   * Alter worker.
   * @param NodeInterface $worker
   * @param array $params
   */
  public function alterWorker(UserInterface $worker, array $params);
  
  /**
   * Delete user.
   * @param UserInterface $worker
   */
  public function deleteUser(UserInterface $worker);
  
  /**
   * Return all signings for a worker
   * @param UserInterface $worker
   * @return bool
   */
  public function hasWorkerSignings(UserInterface $worker);
  
  /**
   * Return all calendars.
   * @return array Return an empty array if no calendars are founds.
   */
  public function getCalendars(int $status = 1);
  
  /**
   * Add Calendar.
   * @param array $params
   * @return NodeInterface
   */
  public function addCalendar(array $params);
  
  /**
   * Alter Calendar.
   * @param NodeInterface $calendar
   * @param array $params
   */
  public function alterCalendar(NodeInterface $calendar, array $params);
  
  /**
   * Return true if there are any worker assigned to this calendar.
   * @param NodeInterface $calendar
   * @return bool
   */
  public function hasCalendarWorkers(NodeInterface $calendar);
  
  /**
   * Return true if there are any range date assigned to this calendar.
   * @param NodeInterface $calendar
   * @return bool
   */
  public function hasCalendarDatesRange(NodeInterface $calendar);
  
  /**
   * Return all ranges date for a work calendar.
   * @param NodeInterface $calendar
   * @return array Return an empty array if no DateRanges are founds.
   */
  public function getDateRanges(NodeInterface $calendar);
  
  /**
   * Add range date.
   * @param NodeInterface $calendar
   * @return NodeInterface
   */
  public function addDateRange(NodeInterface $calendar, array $params);
  
  /**
   * Alter range date.
   * @param NodeInterface $daterange
   * @param array $params
   */
  public function alterDateRange(NodeInterface $daterange, array $params);
  
  /**
   * Check if exist collision with other date range.
   * @param int $date_start
   * @param int $date_end
   * @param int $priority
   * @return bool
   */
  public function hasDateRangeCollision(NodeInterface $calendar, 
      NodeInterface $daterange = null, int $date_start, int $date_end, 
      int $priority);
  
  /**
   * Return user by id.
   * @param int $id
   * @return UserInterface|NULL Return null if no user is found.
   */
  public function getUserById(int $id);
  
  /**
   * Return signings between date_start and date_end.
   * @param UserInterface $worker
   * @param int $date_start
   * @param int $date_end
   * @return array Return an empty array if no signings are founds.
   */
  public function getSignings(UserInterface $worker, int $date_start, int $date_end);
  
  /**
   * Return signing.
   * @param array $params
   * @return NodeInterface
   */
  public function addSigning(array $params);
  
  /**
   * Return node.
   * @param int $nid
   * @return NodeInterface|NULL
   */
  public function getNode(int $id);
  
  /**
   * Return worktime sum for all signings.
   * @param array $signings
   * @return int Return timestamp that represent accumulated time for all signings.
   */
  public function getWorkTimePerDay(array $signings);
  
  /**
   * Return theoretical work time for a user in a day.
   * @param Userinterface $worker
   * @param int $unixtimestamp
   * @return int Return timestamp with time or cero if no date range is found.
   */
  public function getTheoreticalTimePerDay(UserInterface $worker, int $unixtimestamp);
  
  /**
   * Return date range for a worker in a time period.
   * @param UserInterface $worker
   * @param int $date
   * @return NodeInterface|NULL 
   */
  public function getDateRangeForDay(UserInterface $worker, int $date);
  
  /**
   * Return total work time by worker in a time period.
   * @param UserInterface $worker
   * @param int $date_start
   * @param int $date_end
   * @return int Return timestamp that represent accumulated worktime between date start an date end.
   */
  public function getWorkTimePerPeriod(UserInterface $worker, int $date_start, int $date_end);
  
  /**
   * Return total theoretical time by worker in a time period.
   * @param UserInterface $worker
   * @param int $date_start
   * @param int $date_end
   * @return int Return timestamp that represent accumulated theoretical worktime between date start and date end.
   */
  public function getTheoreticalTimePerPeriod(UserInterface $worker, int $date_start, int $date_end);
}
<?php
namespace Drupal\efichajes;

use Drupal\user\UserInterface;

interface EfichajesFormatterInterface {
  /**
   * Return signing types table.
   * @param array $signingtypes
   * @return array
   */
  public function getSigningTypesTable(array $signingtypes);
  
  /**
   * Return workers table.
   * @param array $workers
   * @return array
   */
  public function getWorkersTable(array $workers);
  
  /**
   * Return calendars table.
   * @param array $calendars
   * @return array
   */
  public function getCalendarsTable(array $calendars);
  
  /**
   * Return ranges date table.
   * @param array $rangesdate
   * @return array
   */
  public function getDatesRangeTable(array $daterange);
  
  /**
   * Return signings table.
   * @param array $signings
   * @return array
   */
  public function getSigningsDayResumeTable(array $signings);
  
  /**
   * Return signing list table.
   * @param UserInterface $worker
   * @param int $date_start
   * @param int $date_end
   * @return array
   */
  public function getSigningsListTable(UserInterface $worker, int $date_start,
      int $date_end);
  
  /**
   * Return format string per timestamp.
   * @param int $timestamp
   * @return string
   */
  public function getTimeString(int $timestamp);
}
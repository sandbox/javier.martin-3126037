<?php
namespace Drupal\efichajes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

class FormController extends ControllerBase {
  protected $form_builder;
  protected $current_user;
  
  /**
   * Implementation construct method.
   * @param FormBuilderInterface $form_builder
   */
  public function __construct(FormBuilderInterface $form_builder,
      AccountProxyInterface $current_user) {
        $this->form_builder = $form_builder;
        $this->current_user = $current_user;
  }
  
  /**
   * Implementation create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Controller\FormController
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('form_builder'),
        $container->get('current_user')
    );
  }
  
  /**
   * Implementation signing type controller.
   * @param String $operation
   * @param NodeInterface $signingtype
   * @return array
   */
  public function signingType(String $operation, NodeInterface $signingtype = null) {
    switch ($operation) {
      case 'add':
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\SigningTypeManagerForm', $signingtype);
        break;
      case 'list':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\SigningTypeListForm');
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\SigningTypeDeleteForm', $signingtype);
        break;
    }
    
    return $form;
  }
  
  /**
   * Implementation work calendar controller.
   * @param String $operation
   * @param NodeInterface $workcalendar
   * @return array
   */
  public function calendar(String $operation, NodeInterface $calendar = null) {
    switch ($operation) {
      case 'add':
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\CalendarManagerForm', $calendar);
        break;
      case 'list':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\CalendarListForm');
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\CalendarDeleteForm', $calendar);
        break;
    }
    
    return $form;
  }
  
  /**
   * Implementation ranges date controller.
   * @param String $operation
   * @param NodeInterface $workcalendar
   * @param NodeInterface $rangesdate
   */
  public function dateRange(String $operation, NodeInterface $calendar = null, NodeInterface $daterange = null) {
    switch ($operation) {
      case 'list':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\DateRangeListForm', $calendar);
        break;
      case 'add':
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\DateRangeManagerForm', $calendar, $daterange);
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\DateRangeDeleteForm', $daterange);
        break;
    }
    
    return $form;
  }
  
  /**
   * Implementation workers controller.
   * @param String $operation
   * @param NodeInterface $worker
   * @return array
   */
  public function worker(String $operation, UserInterface $worker = null) {
    switch($operation) {
      case 'add':
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\WorkerManagerForm', $worker);
        break;
      case 'list':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\WorkerListForm');
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\WorkerDeleteForm', $worker);
        break;
      case 'list-signings':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\SigningListForm', $worker);
        break;
    }
    
    return $form;
  }
}
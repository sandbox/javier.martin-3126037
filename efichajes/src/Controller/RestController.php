<?php
namespace Drupal\efichajes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\efichajes\EfichajesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RestController extends ControllerBase {
  protected $current_user;
  protected $efichajesManager;
  
  /**
   * Implementation construct function.
   * @param AccountProxyInterface $current_user
   * @param EfichajesManagerInterface $efichajesManager
   */
  public function __construct(AccountProxyInterface $current_user, 
      EfichajesManagerInterface $efichajesManager) {
    $this->current_user = $current_user;
    $this->efichajesManager = $efichajesManager;
  }
  
  /**
   * Implementation function create
   * @param ContainerInterface $container
   * @return RestController
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('efichajes.manager')
    );
  }
  
  /**
   * Return all workers.
   */
  public function getWorkers() {
    $result = [];
    $workers = $this->efichajesManager->getWorkers();
    
    if (empty($workers)) {
      $result['status'] = 'OK';
      $result['message'] = 'No workers found.';
    } else {
      $result['status'] = 'OK';
      $result['message'] = 'Workers returned';
      $result['values'] = [];
      foreach ($workers as $key => $value) {
        $result['values'][$key]['id'] = $value->get('field_ef_id')->value;
        $result['values'][$key]['name'] = $value->get('field_ef_name')->value;
        $result['values'][$key]['surname'] = $value->get('field_ef_surname')->value;
        $result['values'][$key]['mail'] = $value->getEMail();
        $result['values'][$key]['phone'] = $value->get('field_ef_phone')->value;
      }
    }
    
    $response = new JsonResponse($result);
    return $response;
  }
  
  /**
   * Return all signings for a worker.
   * @param int $nid
   * @param int $date_start
   * @param int $date_end
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getSignings(Request $request) {
    $result = [];
    $nid = $request->query->get('nid');
    $date_start = $request->query->get('date_start');
    $date_end = $request->query->get('date_end');
    
    
    if (isset($nid) && (is_numeric($nid)) && 
        isset($date_start) && (is_numeric($date_start)) &&
        isset($date_end) && (is_numeric($date_end)))
    {
      $worker = $this->efichajesManager->getUserById($nid);
    } else {
      $worker = null;
      $result['status'] = 'ERROR';
      $result['message'] = 'Error found validating parameters.';
    }
    
    if ((isset($worker)) && ($worker->hasRole('efichajes_worker')) ) {
      $signings = $this->efichajesManager->getSignings($worker, $date_start, $date_end);
      if (empty($signings)) {
        $result['status'] = 'OK';
        $result['message'] = 'No signings found.';
      } else {
        $result['status'] = 'OK';
        $result['message'] = 'Signings returned.';
        $result['values'] = [];
        foreach ($signings as $key => $value) {
          $result['values'][$key]['nid'] = $value->id();
          $result['values'][$key]['date'] = $value->get('field_ef_date')->value;
          $result['values'][$key]['data'] = $value->get('field_ef_data')->value;
          $result['values'][$key]['ip'] = $value->get('field_ef_ip')->value;
        }
      }
    } else {
      if (!array_key_exists('status', $result)) {
        $result['status'] = 'OK';
        $result['message'] = 'No worker found.';
      }
    }

    $response = new JsonResponse($result);
    return $response;
  }
  
}